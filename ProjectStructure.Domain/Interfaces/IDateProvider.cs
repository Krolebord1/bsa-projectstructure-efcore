﻿using System;

namespace ProjectStructure.Domain.Interfaces
{
    public interface IDateProvider
    {
        public DateTimeOffset Now { get; }
    }
}
