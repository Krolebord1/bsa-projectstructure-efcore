﻿namespace ProjectStructure.Domain.Interfaces
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
