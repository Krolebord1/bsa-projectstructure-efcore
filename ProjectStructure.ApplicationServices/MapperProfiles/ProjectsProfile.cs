﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ApplicationServices.MapperProfiles
{
    public class ProjectsProfile : Profile
    {
        public ProjectsProfile()
        {
            CreateMap<Project, ProjectReadDTO>().ReverseMap();
            CreateMap<ProjectWriteDTO, Project>();
        }
    }
}
