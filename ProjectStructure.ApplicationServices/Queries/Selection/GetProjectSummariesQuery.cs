﻿using System.Collections.Generic;
using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Projects;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetProjectSummariesQuery : IRequest<IEnumerable<ProjectSummaryDTO>>
    {
        public int MinProjectDescriptionLength => 20;

        public int MaxProjectTasksCount => 3;
    }
}
