﻿using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetUserSummaryQuery : IRequest<OneOf<UserSummaryDTO, NotFound>>
    {
        public int UserId { get; }

        public GetUserSummaryQuery(int userId)
        {
            UserId = userId;
        }
    }
}
