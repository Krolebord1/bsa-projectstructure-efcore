﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Tasks;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Tasks
{
    public class UpdateTaskHandler : IRequestHandler<UpdateTaskCommand, OneOf<Success, NotFound>>
    {
    private readonly IRepository<UserTask> _tasksRepository;
    private readonly IMapper _mapper;

    public UpdateTaskHandler(IRepository<UserTask> tasksRepository, IMapper mapper)
    {
        _tasksRepository = tasksRepository;
        _mapper = mapper;
    }

    public async Task<OneOf<Success, NotFound>> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
    {
        var task = await _tasksRepository.ReadAsync(request.Id);

        if (task == null)
            return new NotFound();

        _mapper.Map(request.TaskDTO, task);

        await _tasksRepository.SaveChangesAsync();

        return new Success();
    }
    }
}
