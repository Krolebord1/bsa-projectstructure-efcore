﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetUserTaskCountHandler : IRequestHandler<GetUserTaskCountQuery, IEnumerable<KeyValuePair<ProjectReadDTO, int>>>
    {
        private IReadRepository<Project> _projectsRepository;
        private IMapper _mapper;

        public GetUserTaskCountHandler(IReadRepository<Project> projectsRepository, IMapper mapper)
        {
            _projectsRepository = projectsRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<KeyValuePair<ProjectReadDTO, int>>> Handle(GetUserTaskCountQuery request, CancellationToken cancellationToken)
        {
            var projects = await _projectsRepository.ReadQuery()
                .Where(x => x.AuthorId == request.UserId)
                .Include(x => x.Tasks)
                .ToListAsync();

            return projects
                .Select(project => new KeyValuePair<ProjectReadDTO, int>(
                        key: _mapper.Map<Project, ProjectReadDTO>(project),
                        value: project.Tasks.Count(task => task.PerformerId == request.UserId)
                    )
                );
        }
    }
}
