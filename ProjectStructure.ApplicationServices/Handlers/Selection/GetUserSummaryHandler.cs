﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetUserSummaryHandler : IRequestHandler<GetUserSummaryQuery, OneOf<UserSummaryDTO, NotFound>>
    {
        private readonly IReadRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public GetUserSummaryHandler(IReadRepository<User> usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<OneOf<UserSummaryDTO, NotFound>> Handle(GetUserSummaryQuery request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.ReadQuery()
                .Where(x => x.Id == request.UserId)
                .Include(x => x.Tasks)
                .ThenInclude(x => x.Project)
                .FirstOrDefaultAsync();

            if (user == null)
                return new NotFound();

            var lastProject = user.Tasks
                .Where(task => task.FinishedAt != null)
                .OrderByDescending(task => task.FinishedAt!.Value)
                .FirstOrDefault()?
                .Project;

            var longestTask = user.Tasks
                .Where(task => task.FinishedAt != null)
                .OrderByDescending(task => task.FinishedAt!.Value.Ticks - task.CreatedAt.Ticks)
                .FirstOrDefault();

            return new UserSummaryDTO(
                _mapper.Map<User, UserReadDTO>(user),
                lastProject != null ? _mapper.Map<Project, ProjectReadDTO>(lastProject) : null,
                lastProject != null ? user.Tasks.Count(task => task.ProjectId == lastProject.Id) : 0,
                user.Tasks.Count(task => task.FinishedAt == null),
                longestTask != null ? _mapper.Map<UserTask, TaskReadDTO>(longestTask) : null
            );
        }
    }
}
