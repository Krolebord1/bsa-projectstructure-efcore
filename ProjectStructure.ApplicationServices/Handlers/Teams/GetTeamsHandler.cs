﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Teams
{
    public class GetTeamsHandler : GetEntitiesHandler<Team, TeamReadDTO>
    {
        public GetTeamsHandler(IReadRepository<Team> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
