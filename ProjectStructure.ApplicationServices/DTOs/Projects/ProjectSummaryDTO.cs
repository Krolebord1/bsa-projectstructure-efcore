﻿using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.DTOs.Projects
{
    public record ProjectSummaryDTO(
        ProjectReadDTO Project,
        TaskReadDTO? LongestTask,
        TaskReadDTO? ShortestTask,
        int? UsersCount
    )
    {
        public override string ToString() =>
            $"Project: {Project.Name}" +
            $"\n\tLongest task: {LongestTask?.Name ?? "none"}" +
            $"\n\tShortest task: {ShortestTask?.Name ?? "none"}" +
            (UsersCount != null ? $"\n\tUsers count: {UsersCount.ToString()}" : string.Empty);
    }
}
