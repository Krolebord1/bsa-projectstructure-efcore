﻿using MediatR;
using OneOf;
using OneOf.Types;

namespace ProjectStructure.ApplicationServices.Commands.Projects
{
    public class DeleteProjectCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public DeleteProjectCommand(int id)
        {
            Id = id;
        }
    }
}
