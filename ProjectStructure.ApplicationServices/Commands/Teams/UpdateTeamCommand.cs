﻿using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs.Teams;

namespace ProjectStructure.ApplicationServices.Commands.Teams
{
    public class UpdateTeamCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public TeamWriteDTO TeamDTO { get; }

        public UpdateTeamCommand(int id, TeamWriteDTO teamDTO)
        {
            Id = id;
            TeamDTO = teamDTO;
        }
    }
}
