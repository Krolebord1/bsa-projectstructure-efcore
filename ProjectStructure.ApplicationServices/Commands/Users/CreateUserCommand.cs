﻿using MediatR;
using ProjectStructure.ApplicationServices.DTOs;

namespace ProjectStructure.ApplicationServices.Commands.Users
{
    public class CreateUserCommand : IRequest
    {
        public UserWriteDTO UserDTO { get; }

        public CreateUserCommand(UserWriteDTO userDTO)
        {
            UserDTO = userDTO;
        }
    }
}
