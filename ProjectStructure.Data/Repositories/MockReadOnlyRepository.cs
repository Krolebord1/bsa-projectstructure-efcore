﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Data.Interfaces;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data.Repositories
{
    public class MockReadOnlyRepository<TEntity> : IReadRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected IList<TEntity> entities;

        public MockReadOnlyRepository(IDbContext context)
        {
            entities = context.GetEntities<TEntity>();
        }

        public IQueryable<TEntity> ReadQuery() =>
            entities.AsQueryable();

        public Task<TEntity?> ReadAsync(int id) =>
            Task.FromResult(entities.FirstOrDefault(entity => entity.Id == id));

        public Task<IList<TEntity>> ReadAllAsync() =>
            Task.FromResult(entities);
    }
}
