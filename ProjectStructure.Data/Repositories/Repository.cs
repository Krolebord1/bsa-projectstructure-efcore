﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data.Repositories
{
    public class Repository<TEntity> : ReadOnlyRepository<TEntity>, IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly AppContext _context;

        public Repository(AppContext context, ILogger<Repository<TEntity>> logger) : base(context)
        {
            _context = context;
        }

        public IQueryable<TEntity> WriteQuery() =>
            entities.AsQueryable();

        public async Task<TEntity?> GetAsync(int id)
        {
            return await WriteQuery().FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public async Task<IList<TEntity>> GetAllAsync()
        {
            return await WriteQuery().ToListAsync();
        }

        public TEntity Add(TEntity entity)
        {
            return entities.Add(entity).Entity;
        }

        public TEntity Delete(TEntity entity)
        {
            return entities.Remove(entity).Entity;
        }

        public Task<int> SaveChangesAsync() =>
            _context.SaveChangesAsync();
    }
}
