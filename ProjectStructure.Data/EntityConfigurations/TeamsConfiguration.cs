﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.Data.EntityConfigurationExtensions;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.EntityConfigurations
{
    public class TeamsConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.ToTable("Teams");

            builder.HasKey(team => team.Id);

            builder.Property(team => team.Name)
                .IsName();
        }
    }
}
