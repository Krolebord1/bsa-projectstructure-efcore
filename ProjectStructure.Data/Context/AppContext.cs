﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProjectStructure.Data.Seed;
using ProjectStructure.Data.Seed.Services;

namespace ProjectStructure.Data
{
    public class AppContext : DbContext
    {
        private readonly IConfiguration _configuration;

        private readonly ISeedingProvider _seedingProvider;

        public AppContext(DbContextOptions options, IConfiguration configuration, ISeedingProvider seeding) : base(options)
        {
            _configuration = configuration;

            _seedingProvider = seeding;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("MSSQLConnection"));

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppContext).Assembly);

            modelBuilder.Seed(_seedingProvider);

            base.OnModelCreating(modelBuilder);
        }
    }
}
