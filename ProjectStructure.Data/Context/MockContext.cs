﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Data.Interfaces;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Data
{
    public class MockContext : IDbContext
    {
        private readonly IList<User> _users = new List<User>();

        private readonly IList<Project> _projects = new List<Project>();

        private readonly IList<Team> _teams = new List<Team>();

        private readonly IList<UserTask> _tasks = new List<UserTask>();

        public IList<TEntity> GetEntities<TEntity>() where TEntity : class, IEntity
        {
            if (typeof(TEntity) == typeof(User))
                return (IList<TEntity>)_users;

            if (typeof(TEntity) == typeof(Project))
                return (IList<TEntity>)_projects;

            if (typeof(TEntity) == typeof(Team))
                return (IList<TEntity>)_teams;

            if (typeof(TEntity) == typeof(UserTask))
                return (IList<TEntity>)_tasks;

            throw new NotRegisteredEntityException(typeof(TEntity));
        }

        public Task<int> SaveChangesAsync()
        {
            _users.Join(
                _teams,
                user => user.TeamId,
                team => team.Id,
                (user, team) =>
                {
                    team.Users.Add(user);
                    user.Team = team;
                    return user;
                }
            ).Apply();

            _users.GroupJoin(
                _tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, userTasks) =>
                {
                    user.Tasks = userTasks
                        .Select(task =>
                        {
                            task.Performer = user;
                            return task;
                        })
                        .ToList();
                    return user;
                }
            ).Apply();

            _projects.Join(
                _users,
                project => project.AuthorId,
                user => user.Id,
                (project, user) =>
                {
                    project.Author = user;
                    return project;
                }
            ).Apply();

            _projects.Join(
                _teams,
                project => project.TeamId,
                team => team.Id,
                (project, team) =>
                {
                    team.Projects.Add(project);
                    project.Team = team;
                    return project;
                }
            ).Apply();

            _projects.GroupJoin(
                _tasks,
                project => project.Id,
                task => task.ProjectId,
                (project, projectTasks) =>
                {
                    project.Tasks = projectTasks
                        .Select(task =>
                        {
                            task.Project = project;
                            return task;
                        })
                        .ToList();
                    return project;
                }
            ).Apply();

            return Task.FromResult(1);
        }
    }

    public class NotRegisteredEntityException : Exception
    {
        public NotRegisteredEntityException(Type type) : base($"Type: <{type.FullName}> is not registered inside context") {}
    }

    public static class LinqExtensions
    {
        public static void Apply<TSource>(this IEnumerable<TSource> enumerable)
        {
            var unused = enumerable.Count();
        }
    }
}
