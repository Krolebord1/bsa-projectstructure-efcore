﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.Data.EntityConfigurationExtensions
{
    public static class NonNullableStringExtension
    {
        public static PropertyBuilder<string> IsNonNullableString(this PropertyBuilder<string> property) =>
            property
                .IsRequired()
                .HasDefaultValue(string.Empty);
    }
}
