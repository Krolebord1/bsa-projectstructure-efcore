﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.Data.Repositories;
using ProjectStructure.Data.Seed.Options;
using ProjectStructure.Data.Seed.Services;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data
{
    public static class RegisterDataAccessLayer
    {
        public static IServiceCollection AddDataLayer(this IServiceCollection services, IConfiguration configuration)
        {
            var updatedConfiguration = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("seedOptions.json")
                .Build();

            services.AddSingleton<IConfiguration>(updatedConfiguration);

            services.AddOptions<SeedOptions>().BindConfiguration(SeedOptions.Key);

            services.AddTransient<IDataLoader, DataLoader>();
            services.AddTransient<ISeedingProvider, SeedingProvider>();

            services.AddDbContext<AppContext>();

            services.AddRepository<User, Repository<User>>();
            services.AddRepository<Project, Repository<Project>>();
            services.AddRepository<Team, Repository<Team>>();
            services.AddRepository<UserTask, Repository<UserTask>>();

            return services;
        }

        private static void AddRepository<TEntity, TImplementation>(this IServiceCollection services)
            where TEntity : class, IEntity
            where TImplementation : class, IReadRepository<TEntity>, IWriteRepository<TEntity>, IRepository<TEntity>
        {
            services.AddTransient<IReadRepository<TEntity>>(provider => provider.GetService<IRepository<TEntity>>()!);

            services.AddTransient<IWriteRepository<TEntity>>(provider => provider.GetService<IRepository<TEntity>>()!);

            services.AddTransient<IRepository<TEntity>, TImplementation>();
        }
    }
}
