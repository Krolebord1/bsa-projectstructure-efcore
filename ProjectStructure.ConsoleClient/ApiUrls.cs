﻿namespace ProjectStructure.ConsoleClient
{
    public static class ApiUrls
    {
        public static readonly string Scheme = "https";
        public static readonly string Authority = "localhost:5001";
        public static readonly string ApiPath = "api";

        public static readonly string FullApiPath = $"{Scheme}://{Authority}/{ApiPath}";

        public static string AllProjects() => $"{FullApiPath}/projects";
        public static string SpecifiedProject(int id) => $"{FullApiPath}/projects/{id}";

        public static string AllTasks() => $"{FullApiPath}/tasks";
        public static string SpecifiedTask(int id) => $"{FullApiPath}/tasks/{id}";

        public static string AllTeams() => $"{FullApiPath}/teams";
        public static string SpecifiedTeam(int id) => $"{FullApiPath}/team/{id}";

        public static string AllUsers() => $"{FullApiPath}/users";
        public static string SpecifiedUser(int id) => $"{FullApiPath}/users/{id}";

        public static string GetUserTasksCount(int userId) =>
            $"{FullApiPath}/selections/users/{userId}/tasks/count";

        public static string GetUserTasks(int userId) =>
            $"{FullApiPath}/selections/users/{userId}/tasks";

        public static string GetUserTasksFinishedInCurrentYear(int userId) =>
            $"{FullApiPath}/selections/users/{userId}/tasks/finished-in-current-year";

        public static string GetTeamsWithOldEnoughUsers() =>
            $"{FullApiPath}/selections/teams/old-enough-teams";

        public static string GetSortedUsers() =>
            $"{FullApiPath}/selections/users/sorted";

        public static string GetUserSummary(int userId) =>
            $"{FullApiPath}/selections/users/{userId}/summary";

        public static string GetProjectSummary() =>
            $"{FullApiPath}/selections/projects/summaries";
    }
}
