﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.ConsoleClient.Interfaces;
using ProjectStructure.ConsoleClient.Programs;
using ProjectStructure.ConsoleClient.Services;

namespace ProjectStructure.ConsoleClient
{
    public static class DI
    {
        private static readonly IServiceProvider Provider;

        static DI()
        {
            var services = new ServiceCollection();

            services.AddTransient<IDataLoader, DataLoader>();

            services.AddSingleton<IDataProcessor, DataProcessor>();
            services.AddSingleton<IAsyncProgram, MainProgram>();

            services.AddAutoMapper(typeof(ApplicationServices.Services).Assembly);

            Provider = services.BuildServiceProvider();
        }

        public static T GetService<T>() =>
            Provider.GetService<T>() ?? throw new DependencyInjectionException(typeof(T));
    }

    public class DependencyInjectionException : Exception
    {
        public DependencyInjectionException(Type type)
            : base($"Type <{type.FullName} is not registered inside DI container>") {}
    }
}
