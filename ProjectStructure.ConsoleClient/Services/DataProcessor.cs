﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public class DataProcessor : IDataProcessor
    {
        private readonly IDataLoader _loader;

        public DataProcessor(IDataLoader loader)
        {
            _loader = loader;
        }

        public void Dispose()
        {
            _loader.Dispose();
        }

        public async Task<IDictionary<ProjectReadDTO, int>> GetUserTasksCount(int userId)
        {
            var result =
                await _loader.GetDeserializedAsync<IEnumerable<KeyValuePair<ProjectReadDTO, int>>>(ApiUrls.GetUserTasksCount(userId))
                ?? new Dictionary<ProjectReadDTO, int>();

            return result.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public async Task<IEnumerable<TaskReadDTO>> GetUserTasks(int userId)
        {
            return await _loader.GetDeserializedAsync<IEnumerable<TaskReadDTO>>(ApiUrls.GetUserTasks(userId))
                   ?? new List<TaskReadDTO>();
        }

        public async Task<IEnumerable<(int, string)>> GetUserTasksFinishedInCurrentYear(int userId)
        {
            return await _loader.GetDeserializedAsync<IEnumerable<(int, string)>>(ApiUrls.GetUserTasksFinishedInCurrentYear(userId))
                   ?? new List<(int, string)>();
        }

        public async Task<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>> GetTeamsWithOldEnoughUsers()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>>(ApiUrls.GetTeamsWithOldEnoughUsers())
                   ?? new List<(int, string, IEnumerable<UserReadDTO>)>();
        }

        public async Task<IEnumerable<UserReadDTO>> GetSortedUsers()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<UserReadDTO>>(ApiUrls.GetSortedUsers())
                ?? new List<UserReadDTO>();
        }

        public async Task<UserSummaryDTO?> GetUserSummary(int userId)
        {
            return await _loader.GetDeserializedAsync<UserSummaryDTO>(ApiUrls.GetUserSummary(userId));
        }

        public async Task<IEnumerable<UserSummaryDTO>> GetProjectSummary()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<UserSummaryDTO>>(ApiUrls.GetProjectSummary())
                   ?? new List<UserSummaryDTO>();
        }
    }
}
