﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public interface IDataProcessor : IDisposable
    {
        Task<IDictionary<ProjectReadDTO, int>> GetUserTasksCount(int userId);

        Task<IEnumerable<TaskReadDTO>> GetUserTasks(int userId);

        Task<IEnumerable<(int, string)>> GetUserTasksFinishedInCurrentYear(int userId);

        Task<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>> GetTeamsWithOldEnoughUsers();

        Task<IEnumerable<UserReadDTO>> GetSortedUsers();

        Task<UserSummaryDTO?> GetUserSummary(int userId);

        Task<IEnumerable<UserSummaryDTO>> GetProjectSummary();
    }
}
