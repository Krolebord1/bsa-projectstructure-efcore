﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ConsoleClient.Attributes;
using ProjectStructure.ConsoleClient.Services;

namespace ProjectStructure.ConsoleClient.Programs
{
    public class MainProgram : ConsoleProgramBase
    {
        private readonly IDataProcessor _dataProcessor;

        public MainProgram(IDataProcessor dataProcessor)
        {
            _dataProcessor = dataProcessor;
        }

        [ProgramEntry(nameof(GetUserTasksCount))]
        public async Task GetUserTasksCount()
        {
            if (!TryGetUserId(out int userId))
                return;

            var (success, result) = await SafeTask(_dataProcessor.GetUserTasksCount(userId));

            if(!success)
                return;

            Console.WriteLine("Tasks count (Only projects with tasks count > 0 are shown):");

            foreach (var pair in result!.Where(pair => pair.Value > 0))
                Console.WriteLine($"{pair.Key.Name}: {pair.Value}");

            WaitForKey();
        }

        [ProgramEntry(nameof(GetUserTasks))]
        public async Task GetUserTasks()
        {
            if (!TryGetUserId(out int userId))
                return;

            var (success, result) = await SafeTask(_dataProcessor.GetUserTasks(userId));

            if(!success)
                return;

            Console.WriteLine("Tasks:");

            foreach (var task in result!)
            {
                Console.WriteLine($"Name: {task.Name}\n\t Description: {task.Description}");
                Console.WriteLine();
            }

            WaitForKey();
        }

        [ProgramEntry(nameof(GetUserTasksFinishedInCurrentYear))]
        public async Task GetUserTasksFinishedInCurrentYear()
        {
            if (!TryGetUserId(out int userId))
                return;

            var (success, result) = await SafeTask(_dataProcessor.GetUserTasksFinishedInCurrentYear(userId));

            if(!success)
                return;

            Console.WriteLine("Tasks:");

            foreach ((int, string) taskInfo in result!)
                Console.WriteLine($"Id: {taskInfo.Item1} Name: {taskInfo.Item2}");

            WaitForKey();
        }

        [ProgramEntry(nameof(GetTeamsWithOldEnoughUsers))]
        public async Task GetTeamsWithOldEnoughUsers()
        {
            var (success, result) = await SafeTask(_dataProcessor.GetTeamsWithOldEnoughUsers());

            if(!success)
                return;

            Console.WriteLine("Teams:");

            foreach ((int, string, IEnumerable<UserReadDTO>) teamInfo in result!)
            {
                Console.WriteLine($"Id: {teamInfo.Item1} Name: {teamInfo.Item2}");

                Console.WriteLine("Users:");
                foreach (var user in teamInfo.Item3)
                    Console.WriteLine($"{user.FirstName} {user.LastName}");

                Console.WriteLine();
            }

            WaitForKey();
        }

        [ProgramEntry(nameof(GetSortedUsers))]
        public async Task GetSortedUsers()
        {
            var (success, result) = await SafeTask(_dataProcessor.GetSortedUsers());

            if(!success)
                return;

            Console.WriteLine("Users");

            foreach (var user in result!)
                Console.WriteLine($"{user.FirstName} {user.LastName}");

            WaitForKey();
        }

        [ProgramEntry(nameof(GetUserSummary))]
        public async Task GetUserSummary()
        {
            if (!TryGetUserId(out int userId))
                return;

            var (success, summary) = await SafeTask(_dataProcessor.GetUserSummary(userId));

            if(!success)
                return;

            Console.WriteLine(summary?.ToString());

            WaitForKey();
        }

        [ProgramEntry(nameof(GetProjectSummary))]
        public async Task GetProjectSummary()
        {
            var (success, result) = await SafeTask(_dataProcessor.GetProjectSummary());

            if(!success)
                return;

            foreach (var summary in result!)
            {
                Console.WriteLine(summary.ToString());
                Console.WriteLine();
            }

            WaitForKey();
        }

        private bool TryGetUserId(out int id)
        {
            Console.WriteLine("Please enter user Id:");

            string idInput = Console.ReadLine() ?? "";

            if (int.TryParse(idInput, out id))
                return true;

            WriteError("Invalid user id");
            return false;
        }

        private async Task<(bool, T?)> SafeTask<T>(Task<T> task)
        {
            try
            {
                return (true, await task);
            }
            catch (DataLoadingException e)
            {
                WriteError(e.ToString());
                //WriteError("Couldn't load data. Try again later.");
            }

            return (false, default);
        }
    }
}
