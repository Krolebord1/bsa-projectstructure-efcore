﻿using System;
using System.Threading.Tasks;
using ProjectStructure.ConsoleClient.Interfaces;

namespace ProjectStructure.ConsoleClient
{
    static class Program
    {
        public const string AppTitle = "BSA 2021 Task 1";

        static async Task Main()
        {
            Console.Title = AppTitle;

            var app = DI.GetService<IAsyncProgram>();
            await app.RunAsync();
        }
    }
}
