﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries.Selection;

namespace ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route(APIRoutes.SelectionsController)]
    public class SelectionsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SelectionsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("users/{userId:int}/tasks/count")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<ProjectReadDTO, int>>>> GetUserTasksCount([FromRoute] int userId)
        {
            var request = new GetUserTaskCountQuery(userId);
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("users/{userId:int}/tasks")]
        public async Task<ActionResult<IEnumerable<TaskReadDTO>>> GetUserTasks(int userId)
        {
            var request = new GetUserTasksQuery(userId);
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("users/{userId:int}/tasks/finished-in-current-year")]
        public async Task<ActionResult<IEnumerable<(int, string)>>> GetUserTasksFinishedInCurrentYear(int userId)
        {
            var request = new GetUserTasksFinishedInCurrentYearQuery(userId);
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("teams/old-enough-users")]
        public async Task<ActionResult<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>>> GetTeamsWithOldEnoughUsers()
        {
            var request = new GetTeamsWithOldEnoughUsersQuery();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("users/sorted")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetSortedUsers()
        {
            var request = new GetSortedUsersQuery();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("users/{userId:int}/summary")]
        public async Task<ActionResult<UserSummaryDTO>> GetUserSummary(int userId)
        {
            var request = new GetUserSummaryQuery(userId);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                userSummary => Ok(userSummary),
                notFound => NotFound()
            );
        }

        [HttpGet("projects/summaries")]
        public async Task<ActionResult<IEnumerable<ProjectSummaryDTO>>> GetProjectSummaries()
        {
            var request = new GetProjectSummariesQuery();
            var response = await _mediator.Send(request);

            return Ok(response);
        }
    }
}
